// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$AppStateCWProxy {
  AppState count(int? count);

  AppState currentSongId(String? currentSongId);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `AppState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// AppState(...).copyWith(id: 12, name: "My name")
  /// ````
  AppState call({
    int? count,
    String? currentSongId,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfAppState.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfAppState.copyWith.fieldName(...)`
class _$AppStateCWProxyImpl implements _$AppStateCWProxy {
  final AppState _value;

  const _$AppStateCWProxyImpl(this._value);

  @override
  AppState count(int? count) => this(count: count);

  @override
  AppState currentSongId(String? currentSongId) =>
      this(currentSongId: currentSongId);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `AppState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// AppState(...).copyWith(id: 12, name: "My name")
  /// ````
  AppState call({
    Object? count = const $CopyWithPlaceholder(),
    Object? currentSongId = const $CopyWithPlaceholder(),
  }) {
    return AppState(
      count: count == const $CopyWithPlaceholder()
          ? _value.count
          // ignore: cast_nullable_to_non_nullable
          : count as int?,
      currentSongId: currentSongId == const $CopyWithPlaceholder()
          ? _value.currentSongId
          // ignore: cast_nullable_to_non_nullable
          : currentSongId as String?,
    );
  }
}

extension $AppStateCopyWith on AppState {
  /// Returns a callable class that can be used as follows: `instanceOfAppState.copyWith(...)` or like so:`instanceOfAppState.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$AppStateCWProxy get copyWith => _$AppStateCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppState _$AppStateFromJson(Map<String, dynamic> json) => AppState(
      count: json['count'] as int? ?? 0,
      currentSongId: json['currentSongId'] as String? ?? '',
    );

Map<String, dynamic> _$AppStateToJson(AppState instance) => <String, dynamic>{
      'count': instance.count,
      'currentSongId': instance.currentSongId,
    };
