import 'package:soundcloud_clone/redux/state.dart';

class AppActionTypes {
  static const String increment = 'increment';
  static const String decrement = 'decrement';

  const AppActionTypes({increment, decrement});
}

AppState appReducer(AppState state, dynamic action) {
  switch (action) {
    case AppActionTypes.increment:
      return state.copyWith(count: state.count! + 1);
    case AppActionTypes.decrement:
      return state.copyWith(count: state.count! - 1);
    default:
      return state;
  }
}
