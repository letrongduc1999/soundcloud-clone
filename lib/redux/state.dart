import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'state.g.dart';

@immutable
@CopyWith()
@JsonSerializable()
class AppState {
  final int? count;
  final String? currentSongId;
  const AppState({
    this.count = 0,
    this.currentSongId = '',
  });

  factory AppState.initial() {
    return const AppState();
  }

  factory AppState.fromJson(Map<String, dynamic> json) =>
      _$AppStateFromJson(json);

  Map<String, dynamic> toJson() => _$AppStateToJson(this);
}
