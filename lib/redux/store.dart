import 'dart:io';
import 'package:redux/redux.dart';
import 'package:path_provider/path_provider.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:soundcloud_clone/redux/reducer.dart';
import 'package:soundcloud_clone/redux/state.dart';

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

Future<File> get _localFile async {
  final path = await _localPath;
  return File('$path/state.json');
}

Future<Store<AppState>> createStore() async {
  final persistor = Persistor<AppState>(
    storage: FileStorage(await _localFile),
    serializer: JsonSerializer<AppState>((json) {
      return AppState.fromJson(json);
    }),
    debug: true,
  );

  AppState? initialState;
  try {
    initialState = await persistor.load();
  } catch (e) {
    initialState = AppState.initial();
  }

  return Store(
    appReducer,
    initialState: initialState ?? AppState.initial(),
    middleware: [persistor.createMiddleware()],
  );
}
