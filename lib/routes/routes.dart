import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:soundcloud_clone/routes/pages.dart';
import 'package:soundcloud_clone/screens/search/search_screen.dart';
import 'package:soundcloud_clone/screens/home/home_screen.dart';
import 'package:soundcloud_clone/screens/library/library_screen.dart';
import 'package:soundcloud_clone/screens/trending/trending_screen.dart';
import 'package:soundcloud_clone/widgets/bottom_bar.dart';

final router = GoRouter(
    routes: [
      GoRoute(
        path: Pages.home,
        builder: (context, state) => const HomeScreen(),
      ),
      GoRoute(
        path: Pages.search,
        builder: (context, state) => const SearchScreen(),
      ),
      GoRoute(
        path: Pages.trending,
        builder: (context, state) => const TrendingScreen(),
      ),
      GoRoute(
        path: Pages.library,
        builder: (context, state) => const LibraryScreen(),
      ),
    ],
    navigatorBuilder: (context, state, child) => MaterialApp(
          home: Scaffold(body: child, bottomNavigationBar: const BottomBar()),
        ));
