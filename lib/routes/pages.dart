class Pages {
  static const home = '/';
  static const search = '/search';
  static const trending = '/trending';
  static const library = '/library';
}
