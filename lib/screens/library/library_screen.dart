import 'package:flutter/material.dart';
import 'package:soundcloud_clone/widgets/app_bar.dart';

class LibraryScreen extends StatelessWidget {
  const LibraryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Text("Library"),
      appBar: CommonAppBar(title: 'Library'),
    );
  }
}
