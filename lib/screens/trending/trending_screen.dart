import 'package:flutter/material.dart';
import 'package:soundcloud_clone/widgets/app_bar.dart';

class TrendingScreen extends StatelessWidget {
  const TrendingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: CommonAppBar(title: 'Trending'),
      body: Text("Trending"),
    );
  }
}
