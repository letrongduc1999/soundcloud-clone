import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:soundcloud_clone/constants/styles.dart';
import 'package:soundcloud_clone/redux/reducer.dart';
import 'package:soundcloud_clone/redux/state.dart';
import 'package:soundcloud_clone/widgets/app_bar.dart';
import 'package:soundcloud_clone/widgets/bottom_bar.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CommonAppBar(title: 'Home'),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
              style: TextStyle(fontSize: 20),
            ),
            StoreConnector<AppState, String>(
                builder: (context, state) {
                  return Text(
                    state,
                    style: TextStyle(
                        fontSize: Style.text2Xl, color: Style.primaryColor),
                  );
                },
                converter: (store) => store.state.count.toString()),
            StoreConnector<AppState, VoidCallback>(
                builder: ((context, callback) {
              return TextButton(
                onPressed: callback,
                child: const Text("Decrement"),
              );
            }), converter: ((store) {
              return () => store.dispatch(AppActionTypes.decrement);
            })),
            StoreConnector<AppState, VoidCallback>(
                builder: ((context, callback) {
              return TextButton(
                onPressed: callback,
                child: const Text("Increment"),
              );
            }), converter: ((store) {
              return () => store.dispatch(AppActionTypes.increment);
            }))
          ],
        ),
      ),
    );
  }
}
