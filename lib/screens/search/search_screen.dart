import 'package:flutter/material.dart';
import 'package:soundcloud_clone/widgets/app_bar.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      appBar: CommonAppBar(title: 'Search'),
      body: Text("Search"),
    );
  }
}
