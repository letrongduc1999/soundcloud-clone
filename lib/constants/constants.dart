import 'package:flutter/material.dart';
import 'package:soundcloud_clone/models/home_tab.dart';
import 'package:soundcloud_clone/routes/pages.dart';

final navBars = [
  const HomeTab(
      name: 'home',
      routeName: Pages.home,
      activeIcon: Icons.home_filled,
      inactiveIcon: Icons.home_outlined),
  const HomeTab(
      name: 'search',
      routeName: Pages.search,
      activeIcon: Icons.search,
      inactiveIcon: Icons.search_outlined),
  const HomeTab(
      name: 'trending',
      routeName: Pages.trending,
      activeIcon: Icons.play_circle_fill_outlined,
      inactiveIcon: Icons.play_circle_outline),
  const HomeTab(
      name: 'library',
      routeName: Pages.library,
      activeIcon: Icons.library_music_rounded,
      inactiveIcon: Icons.library_music_outlined)
];
