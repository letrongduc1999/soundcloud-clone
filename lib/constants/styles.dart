import 'package:flutter/material.dart';

class Style {
  static Color primaryColor = const Color(0XFFF26F23);
  static Color textColor = Colors.black;
  static Color backgroundColor = Colors.white;
  static Color textSecondaryColor = Colors.grey;

  static double textXs = 12;
  static double textSm = 14;
  static double textBase = 16;
  static double textLg = 20;
  static double textXl = 24;
  static double text2Xl = 28;
}
