import 'package:flutter/cupertino.dart';

class HomeTab {
  final String name;
  final String routeName;
  final IconData activeIcon;
  final IconData inactiveIcon;

  const HomeTab(
      {required this.name,
      required this.routeName,
      required this.activeIcon,
      required this.inactiveIcon});
}
