import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:soundcloud_clone/constants/constants.dart';
import 'package:soundcloud_clone/constants/styles.dart';

class BottomBar extends StatefulWidget {
  const BottomBar({Key? key}) : super(key: key);

  @override
  State<BottomBar> createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  _navigatePage(index) {
    setState(() {
      _currentIndex = index;
    });
    GoRouter.of(context).go(navBars[index].routeName);
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: navBars
          .map((navBar) => BottomNavigationBarItem(
              icon: Icon(navBar.inactiveIcon),
              label: navBar.name,
              activeIcon: Icon(navBar.activeIcon)))
          .toList(),
      selectedItemColor: Style.textColor,
      currentIndex: _currentIndex,
      iconSize: Style.text2Xl,
      type: BottomNavigationBarType.fixed,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      onTap: (value) {
        _navigatePage(value);
      },
    );
  }
}
