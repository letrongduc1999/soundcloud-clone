import 'package:flutter/material.dart';
import 'package:soundcloud_clone/constants/styles.dart';

class CommonAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String title;

  const CommonAppBar({
    Key? key,
    required this.title,
  }) : super(key: key);
  @override
  State<CommonAppBar> createState() => _CommonAppBarState();

  @override
  Size get preferredSize => const Size.fromHeight(60);
}

class _CommonAppBarState extends State<CommonAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        widget.title,
        style: TextStyle(fontSize: Style.text2Xl),
      ),
      centerTitle: true,
    );
  }
}
