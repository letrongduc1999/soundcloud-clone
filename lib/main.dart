import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:path_provider/path_provider.dart';
import 'package:redux/redux.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:soundcloud_clone/redux/reducer.dart';
import 'package:soundcloud_clone/redux/state.dart';
import 'package:soundcloud_clone/redux/store.dart';
import 'package:soundcloud_clone/routes/pages.dart';
import 'package:soundcloud_clone/routes/routes.dart';
import 'package:soundcloud_clone/screens/home/home_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final appStore = await createStore();
  runApp(MyApp(
    store: appStore,
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.store}) : super(key: key);
  final Store<AppState> store;

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
        store: store,
        child: MaterialApp.router(
          routeInformationProvider: router.routeInformationProvider,
          routeInformationParser: router.routeInformationParser,
          routerDelegate: router.routerDelegate,
        ));
  }
}
